<?php

require_once("../config/DbConfig.php");
class User extends DbConfig{

	private $table = "users";
	
    public function execute($query) 
	{
		$result = $this->conn->query($query);
		
		if ($result == false) {
			$result =  'Error: cannot execute the command';
			return $result;
		} else { 
			return $result;
		}		
	}

	public function list(){

		 $email =  $_SESSION['email'];
		 $query = "SELECT * FROM "." $this->table "." WHERE is_deleted = 'N' AND email != '$email'";
    	 $result = $this->execute($query);
	
		 $row = mysqli_fetch_all($result, MYSQLI_ASSOC);
		 return $row;
	}

	public function add()
	{
		$hobbies = json_encode($_POST['hobbies']);
		$tempname = $_FILES["profile_img"]["tmp_name"];
		$filenm = $_FILES['profile_img']['name'];
		$folder = "../public/assets/images/" . $filenm;

		if($filenm==""){
			$filenm = "default.jpg";
		}

		if (isset($_POST)) {
			$emp_exist_query = "SELECT COUNT(id) as cnt FROM users WHERE email = '".$_POST['email']."'";
			$res = $this->execute($emp_exist_query);
			$row = mysqli_fetch_assoc($res);
			
			if($row['cnt']>0){
				// ccd(isset($_GET['add']));
				// echo "<script>alert('Email already exists');</script>";
				if(isset($_GET['register'])){
					echo "Email already exists";
					header("Refresh: 1; URL = ../view/register.php");
					exit();
				}else{
					echo "Email already exists";
					header("Refresh: 1; URL = ../view/add-user.php");
					exit();
				}
			}else{
					$query = "INSERT INTO users (firstname, lastname, email, gender,
												m_no, hobbies, password, designation, profile_img)
												VALUES ('" . $this->validate($_POST['firstname']) . "', 
												'" . $this->validate($_POST['lastname']) . "', 
												'" . $this->validate($_POST['email']) . "',
												'" . $this->validate($_POST['gender']) . "', 
												'" . $this->validate($_POST['m_no']) . "','$hobbies',
												'" . $this->validate($_POST['password']) . "',
												'" . $this->validate($_POST['designation']) . "', 
												'" . $filenm . "');";

					//insert data to database
					$result = $this->execute($query);
					// ccd($result);

					move_uploaded_file($tempname, $folder);

					return $result;
				}
		}
   }

	public function update(){
		
		$tempname = $_FILES["profile_img"]["tmp_name"];
		$filenm = $_FILES['profile_img']['name'];
		$folder = "../public/assets/images/" . $filenm;
		$oldfilenm = $_POST['image_name'];
		$hobbies = json_encode($_POST['hobbies']);
		$firstname =  $_POST['firstname'];
		$lastname =  $_POST['lastname'];
		$email =  $_POST['email'];
		$gender =  $_POST['gender'];
		$m_no =  $_POST['m_no'];
		$designation = $_POST['designation'];
		$uid = $_POST['uid'];
		if ($filenm == "") {
			$filenm = $oldfilenm;
		}
		ccd($hobbies);
		if (isset($_POST)) {
			$query = "UPDATE users SET firstname = '$firstname', 
			lastname = '$lastname', email = '$email', 
			gender = '$gender',  m_no = '$m_no',
			hobbies = '$hobbies',  
			designation = '$designation', 
			profile_img = '$filenm' WHERE id = $uid";
			//insert data to database
			$result = $this->execute($query);
			move_uploaded_file($tempname, $folder);
			return $result;
		}
	}	

	public function delete($uid){
	
		$query = "UPDATE users SET is_deleted = 'Y' WHERE id = $uid ";
		//delete data to UI table 	
		$result = $this->execute($query);
   
		return $result;
   }

   public function getUserDetail($uid){
		$obj = new User();
		$query = "SELECT * FROM users WHERE id
		= '$uid' ";
		$result = $obj->execute($query);
		$row = mysqli_fetch_assoc($result);
		
		// $hobby = json_decode($row['hobbies']);

		return $row;

   }
	public function login(){
		$email = $this->validate($_POST["email"]);
		$password = $this->validate($_POST["password"]);
		$sql= "SELECT * FROM users WHERE email = '$email' AND password = '$password' ";
		$result = $this->execute($sql);
		
		if($row=mysqli_fetch_assoc($result)){
			// ccd($row);
			$_SESSION['login'] = "login successful";
			$_SESSION['firstname'] = $row['firstname'];
			$_SESSION['lastname'] = $row['lastname'];
			$_SESSION['email'] = $row['email'];
			$_SESSION['profile_img'] = $row['profile_img'];

		}else{
			$row = 'ERROR';
		}

		return $row;
	}

	
    function validate($data)
    {
        $data = trim($data);

        $data = stripslashes($data);

        $data = htmlspecialchars($data);

        return $data;
    }
}

?>