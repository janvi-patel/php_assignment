$(document).ready(function () {
    $("form").submit(function (event) {
      
      var formData = {
        password: $("#password").val(),
        email: $("#email").val(),
        
      };
  
    $.ajax({
        type: "POST",
        url: "controller/check_login_info.php",
        data: formData,
        dataType: "json",
        encode: true,
      }).done(function (data) {
        console.log(data);
        
        if (!data.success) {

         
            
          if(data.message!=null){
              $("#login-error").addClass("has-error");
              $("#login-error").append(
                '<div id="login-alrt" class="alert alert-warning alert-dismissible fade show">'+
                  '<strong>'+data.message+'</strong>'+
                  '<button type="button" class="btn-close" data-bs-dismiss="alert"></button>'+
              '</div>'
            );

            
          }

          if (data.errors.password) {
            $("#name-group").addClass("has-error");
            $("#name-group").append(
              '<div class="help-block">' + data.errors.password + "</div>"
            );
          }
  
          if (data.errors.email) {
            $("#email-group").addClass("has-error");
            $("#email-group").append(
              '<div class="help-block">' + data.errors.email + "</div>"
            );
            }

        }
      
        else {
                
                window.location.href = "view/users.php";
            }
  
      });
  
      event.preventDefault();
    });
  


    // ...

  $("form").submit(function (event) {
    $(".form-group").removeClass("has-error");
    $(".help-block").remove();

    // ...
  });

// ...
  });