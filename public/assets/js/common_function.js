$(document).ready(function(){
    $("#checkall").click(function(){
        $('input:checkbox').not(this).prop('checked', this.checked);
             
    });
    
    $('.hobbies').on('click',function(){
        if($('.hobbies:checked').length == $('.hobbies').length){
            $('#checkall').prop('checked',true);
        }else{
            $('#checkall').prop('checked',false);
        }
    });
       
    $("#profile_img").change(function () {
        filePreview(this);
    });

    $('#update_profile_img').change(function(){
        const file = this.files[0];
        console.log(file);
        if (file){
          let reader = new FileReader();
          reader.onload = function(event){
            console.log(event.target.result);
            $('#update_preview').attr('src', event.target.result);
          }
          reader.readAsDataURL(file);
        }
      });
   
});

function filePreview(input) {   
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#preview + img').remove();
            $('#preview').html('<img class="profile-img mt-2" src="'+e.target.result+'"   height="70px" width="70px"/>');
        };
        reader.readAsDataURL(input.files[0]);
    }
}
