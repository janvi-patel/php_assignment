$(document).ready(function(){


  
    $("input").keyup(function () {
        var textValue = $(this).val();
        textValue =textValue.replace(/ /g,"");
        $(this).val(textValue);
      });

      $("#inputPhone").keyup(function () {
        var textValue = $(this).val();
        textValue =textValue.replace(/\D/g, "");
        $(this).val(textValue);
      });

      
    $('#password, #cpassword').on('keyup', function () {
        
        var pw = $('#password').val();
        var cpw = $('#cpassword').val();
        if (pw == cpw && pw != "" && cpw != "") 
        {
                    $('#pw-error').html('Matched').css('color', 'green');
                    $("#enter").prop('disabled',false);
                    return true;
        } 
        else 
                    $('#pw-error').html('Password Missmatch').css('color', 'red');
                     $("#enter").prop('disabled',true);
                     return false;
        });
  
    $('#email').on('keyup',function(){
            var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            var mailformat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
            var email = $("#email").val();
            if(email.match(mailformat))
                $('#email-error').html('');
            
            else
                        $('#email-error').html('Invalid Email').css('color','red');
                
            });

            $('#password').on('blur',function(){
                
                var newPassword = document.getElementById('password').value;
                var minNumberofChars = 6;
                var maxNumberofChars = 16;
                var regularExpression  = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
               
                if(newPassword.length < minNumberofChars || newPassword.length > maxNumberofChars){
                    $("#enter").prop('disabled',true);
                    $('#pw-error').html('enter atleast 6 characters').css('color','red');
                    
                }
                if(!regularExpression.test(newPassword)) {
                    $("#enter").prop('disabled',true);
                    $('#pw-error').html('password should contain atleast one number and one special character').css('color','red');
                    
                }else
                    $("#enter").prop('disabled',false);
            });

            $("#register_form").submit(function(event){
              if($('.hobbies:checked').length == 0){
                alert('Please select atleast one hobby');
                event.preventDefault();
              }
            });  
    });
  
   
//  disabling form submissions if there are invalid fields
(function() {
'use strict';
window.addEventListener('load', function() {
 // Fetch all the forms we want to apply custom Bootstrap validation styles to
 var forms = document.getElementsByClassName('needs-validation');
 
 // Loop over them and prevent submission
 var validation = Array.prototype.filter.call(forms, function(form) {
   form.addEventListener('submit', function(event) {
     if (form.checkValidity() === false) {
       event.preventDefault();
       event.stopPropagation();
     } 
     
     form.classList.add('was-validated');
   }, false);
 });
}, false);
})();

