<?php  error_reporting(1); ?>
<!DOCTYPE html>
<html>
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
      <link rel="stylesheet" href="public/assets/css/style.css">
      <link href="public/assets/css/vendors/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" href="public/assets/fonts/all.min.css"> 
      <title>Log In</title>
   </head>
   <body class="container bg-light">
      <!-- Start Header form -->
      <div class="text-center pt-5">
         <h2>LOGIN FORM</h2>
      </div>
      <!-- End Header form -->
      <!-- Start Card -->
      <div class="card login_card">
         <!-- Start Card Body -->   
         <div class="card-body">
         <form method="post" action="controller/userController.php?login=1" class="needs-validation" novalidate autocomplete="on">
                  <!-- Email input -->
                  <div id="login-error" class="form-group"></div>
                  <div class="form-outline m-4">
                     <label class="h5">Email</label>
                     <input type="email" id="email" class="form-control form-control-lg"
                        placeholder="Enter a valid email address" name="email" required/>
                     <div id="email-group" class="form-group text-danger"></div>
                  </div>
                  <!-- Password input -->
                  <div class="form-outline m-3">
                     <label class="h5">Password</label>
                     <input type="password" id="password" class="form-control form-control-lg"
                        placeholder="Enter password" name="password" required/>
                     <div id="name-group" class="form-group text-danger"></div>
                  </div>
                  <div class="text-center text-lg-start mt-4 pt-2">
                     <button type="submit" class="btn btn-primary btn-lg m-3"
                        style="padding-left: 2.5rem; padding-right: 2.5rem;">Login</button>
                     <p class="small fw-bold mt-2 pt-1 mb-0">Don't have an account? <a href="view/register.php"
                        class="link-danger">Register</a></p>
                  </div>
               </form>
         </div>
         <!-- End Card Body -->
      </div>
      <!-- End Card -->
      <footer>
         <div class="my-4 text-muted text-center">
            <p>© Tridhya Tech Limited</p>
         </div>
      </footer>
      <!-- Optional JavaScript -->
      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
      <script src="public/assets/jquery/jquery-3.6.3.min.js"></script>
      <script src="public/assets/js/vendors/popper.min.js"></script>
      <script src="public/assets/js/vendors/bootstrap.min.js"></script>
      <script src="public/assets/js/vendors/bootstrap.bundle.min.js" rel="text/javascript"></script>
      <script src="public/assets/js/validation.js"></script>
   </body>
</html>