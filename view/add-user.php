<?php
   // Check if the user is logged in, if not then redirect him to login page
   session_start();
   if(!isset($_SESSION['login'])){
      header("Location: ../index.php");
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
      <link rel="stylesheet" href="../public/assets/css/vendors/bootstrap.min.css" />
      <link rel="stylesheet" href="../public/assets/css/style.css" />
      <script src="../public/assets/jquery/jquery-3.6.3.min.js" rel="text/javascript"></script>
      <script src="../public/assets/js/validation.js" rel="text/javascript"></script>
      <script src="../public/assets/js/common_function.js" rel="text/javascript"></script>
      <script src="../public/assets/js/add_user.js" rel="text/javascript"></script>
      <title>Add User</title>
   </head>
   <body class="container bg-light">
      <!-- Start Header form -->
      <div class="text-center pt-5">
         <h2>ADD USER FORM</h2>
      </div>
      <!-- End Header form -->
      <!-- Start Card -->
      <div class="card">
         <!-- Start Card Body -->
         <div class="card-body">
            <!-- Start Form -->
            <form id="register_form" action="../controller/userController.php?add=1" method="post" enctype="multipart/form-data" class="needs-validation" novalidate autocomplete="on">
               <!-- Start Input First Name -->
               <div class="form-group mt-3">
                  <label for="inputName">First Name</label>
                  <input type="text" class="form-control" name="firstname" placeholder="Your first name" required />
               </div>
               <!-- End Input First Name -->
               <!-- Start Input Last Name -->
               <div class="form-group mt-3">
                  <label for="inputName">Last Name</label>
                  <input type="text" class="form-control" name="lastname" placeholder="Your last name" required />
               </div>
               <!-- End Input Last Name -->
               <!-- Start Input Email -->
               <div class="form-group mt-3">
                  <label for="inputEmail">Email</label>
                  <input type="email" class="form-control" id="inputEmail" name="email" placeholder="Enter email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" required />
               </div>
               <!-- End Input Email -->
               <!-- Start Input Password -->
               <div class="row">
                  <div class="col-12">
                     <div class="form-group mt-3">
                        <label>Password</label>
                        <input type="password" class="form-control form-control-sm" name="password" id="password" placeholder="enter password" required>
                        <div id="pw-error" class="form-group"></div>
                     </div>
                  </div>
                  <div class="col-12">
                     <div class="form-group mt-3">
                        <label>Confirm Password</label>
                        <input type="password" class="form-control form-control-sm" name="cpassword" id="cpassword" placeholder="re-enter password" required>
                        <div id="cpw-error" class="form-group"></div>
                     </div>
                  </div>
               </div>
               <!-- End Input Password -->
               <!-- Start Input Telephone -->
               <div class="form-group mt-3">
                  <label for="inputPhone">Phone</label>
                  <input type="tel" class="form-control" id="inputPhone" name="m_no" placeholder="enter 10 digit only" pattern="\d{3}\d{3}\d{4}" required />
               </div>
               <!-- End Input Telephone -->
               <!-- Start Input Gender -->
               <div class="form-group mt-3">
                  <label>Gender</label>
                  <br>
                  <input type="radio" class="gender" id="male" name="gender" value="male" checked>
                  <label class="form-check-label" for="male">Male</label>
                  <input type="radio" class="gender" id="female" name="gender" value="female">
                  <label class="form-check-label" for="female">Female</label>
                  <input type="radio" class="gender" id="other" name="gender" value="other">
                  <label class="form-check-label" for="other">Other</label>
               </div>
               <!-- End Input Gender -->
               <!-- Start Input Hobbies -->
               <div class="form-group mt-3">
                  <label>Hobbies</label><br>
                  <input class="hobbies" type="checkbox" name="hobbies[]" value="dancing" id="dancing">
                  <label class="form-check-label" for="dancing">dancing</label>
                  <input class="hobbies" type="checkbox" name="hobbies[]" value="singing" id="singing">
                  <label class="form-check-label" for="singing">singing</label>
                  <input class="hobbies" type="checkbox" name="hobbies[]" value="travelling" id="travelling">
                  <label class="form-check-label" for="travelling">travelling</label>
                  <input class="hobbies" type="checkbox" name="hobbies[]" value="reading" id="reading">
                  <label class="form-check-label" for="reading">reading</label>
                  <input class="hobbies" type="checkbox" name="hobbies[]"  id="checkall">
                  <label class="form-check-label p-1" for="checkall">check all</label>
                  <div id="hobbies-error" class="form-group text-danger"></div>
               </div>
               <!-- End Input Hobbies -->
               <!-- Start Input Designation -->
               <div class="form-row mt-3">
                  <div class="form-group col-md-4">
                     <label>Designation</label>
                     <div class="d-flex flex-row justify-content-between align-items-center">
                        <select class="form-control mr-1"  name="designation" required>
                           <option value="" disabled selected>please select designation</option>
                           <option value="employee">employee</option>
                           <option value="manager">manager</option>
                           <option value="assistant manager">assistant manager</option>
                           <option value="supervisor">supervisor</option>
                           <option value="hr">hr</option>
                           <option value="intern">intern</option>
                        </select>
                     </div>
                  </div>
                  <!-- End Input Designation -->
               </div>
               <!-- Start Profile Image -->
               <div class="form-group mt-3">
                  <label for="formFile">Profile Image</label>
                  <input class="form-control profile_img" type="file" id="profile_img" accept="images" name="profile_img">
                  Selected Image: <span id="preview" class="mt-2">none</span>   
               </div>
               <!-- End Profile Image -->
               <!-- Start Submit Button -->
               <button class="btn btn-primary btn-block col-lg-2 mt-3" type="submit">Submit</button>
               <!-- End Submit Button -->
            </form>
            <!-- End Form -->
         </div>
         <!-- End Card Body -->
      </div>
      <!-- End Card -->
      <footer>
         <div class="my-4 text-muted text-center">
            <p>© Tridhya Tech Limited</p>
         </div>
      </footer>
      <!-- Optional JavaScript -->
      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
      <script src="../public/assets/js/vendors/bootstrap.bundle.min.js" rel="text/javascript"></script>
      <script src="../public/assets/js/vendors/popper.min.js"></script>
      <script src="../public/assets/js/vendors/bootstrap.min.js"></script>
   </body>
</html>