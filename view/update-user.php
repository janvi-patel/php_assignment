<?php
   // include "../config/DbConfig.php";
   // include "../model/User.php";
   // Check if the user is logged in, if not then redirect him to login page
   if(!isset($_SESSION['login'])){
      header("Location: ../index.php");
   }

 
// $row = $_GET['row'];
// ccd($row);

$row = json_decode($row);
$hobby = json_decode($row->hobbies);

?>
<!DOCTYPE html>
<html>
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="../public/assets/css/vendors/bootstrap.min.css" />
      <link rel="stylesheet" href="../public/assets/css/style.css" />
      <script src="../public/assets/js/vendors/bootstrap.bundle.min.js" rel="text/javascript"></script>
      <script src="../public/assets/jquery/jquery-3.6.3.min.js" rel="text/javascript"></script>
      <script src="../public/assets/js/validation.js" rel="text/javascript"></script>
      <script src="../public/assets/js/common_function.js" rel="text/javascript"></script>
      <title>Update User</title>
   </head>
   <body class="container bg-light">
      <!-- Start Header form -->
      <div class="text-center pt-5">
         <h2>UPDATE USER FORM</h2>
      </div>
      <!-- End Header form -->
      <!-- Start Card -->
      <div class="card">
         <!-- Start Card Body -->
         <div class="card-body">
            <!-- Start Form -->
            <form id="userForm" method="post" action="../controller/userController.php?update=1" class="needs-validation" novalidate autocomplete="on" enctype="multipart/form-data">
               <input type="hidden" value="<?= $row->id ?>" name="uid"/> 
               <!-- Start Input First Name -->
               <div class="form-group mt-3">
                  <label for="inputName">First Name</label>
                  <input type="text" class="form-control" id="inputName" name="firstname" placeholder="Your first name" value="<?php echo $row->firstname ?>" required />
               </div>
               <!-- End Input First Name -->
               <!-- Start Input Last Name -->
               <div class="form-group mt-3">
                  <label for="inputName">Last Name</label>
                  <input type="text" class="form-control" id="inputName" name="lastname" placeholder="Your last name" value="<?php echo $row->lastname ?>" required />
               </div>
               <!-- End Input Last Name -->
               <!-- Start Input Email -->
               <div class="form-group mt-3">
                  <label for="inputEmail">Email</label>
                  <input type="email" class="form-control" id="inputEmail" name="email" placeholder="Enter email" value="<?php echo $row->email ?>" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" required />
                  <div id="email-group" class="form-group text-danger"></div>
               </div>
               <!-- End Input Email -->
               <!-- Start Input Telephone -->
               <div class="form-group mt-3">
                  <label for="inputPhone">Phone</label>
                  <input type="tel" class="form-control" id="inputPhone" name="m_no" placeholder="enter 10 digit only" value="<?php echo $row->m_no ?>" pattern="\d{3}\d{3}\d{4}" required />
               </div>
               <!-- End Input Telephone -->
               <!-- Start Input Gender -->
               <div class="form-group mt-3">
                  <label>Gender</label>
                  <br>
                  <input type="radio" class="gender" id="male" name="gender" value="male" <?php if($row->gender == 'male') {echo "checked"; }  ?>>
                  <label  for="male">Male</label>
                  <input type="radio" class="gender" id="female" name="gender" value="female" <?php if($row->gender == 'female') {echo "checked"; } ?>>
                  <label  for="female">Female</label>
                  <input type="radio" class="gender" id="other" name="gender" value="other" <?php if($row->gender == 'other') {echo "checked"; } ?>>
                  <label  for="other">Other</label>
               </div>
               <!-- End Input Gender -->
               <!-- Start Input Hobbies -->
               <div class="form-group mt-3">
                  <label>Hobbies</label><br>
                  <input class="hobbies" type="checkbox" name="hobbies[]" value="dancing" id="dancing" <?php if(in_array("dancing",$hobby)) {echo "checked"; }  ?>>
                  <label  for="dancing">dancing</label>
                  <input class="hobbies" type="checkbox" name="hobbies[]" value="singing" id="singing" <?php if(in_array("singing",$hobby)) {echo "checked"; }  ?>>
                  <label  for="singing">singing</label>
                  <input class="hobbies" type="checkbox" name="hobbies[]" value="travelling" id="travelling" <?php if(in_array("travelling",$hobby)) {echo "checked"; }  ?>>
                  <label  for="travelling">travelling</label>
                  <input class="hobbies" type="checkbox" name="hobbies[]" value="reading" id="reading" <?php if(in_array("reading",$hobby)) {echo "checked"; }  ?>>
                  <label  for="reading">reading</label>
                  <input class="hobbies" type="checkbox" name="hobbies[]"  id="checkall" <?php if(sizeof($hobby)==4) {echo "checked"; }  ?>>
                  <label class="form-check-label p-1" for="checkall">check all</label>
                  <div id="hobbies-error" class="form-group text-danger"></div>
               </div>
               <!-- End Input Hobbies -->

               <!-- Start Designation -->

               <?php $opt_array = array("employee", "manager", "assistant manager", "supervisor", "hr", "intern"); ?>
               <div class="form-row mt-3">
                  <div class="form-group col-md-4">
                     <label>Designation</label>
                     <div class="d-flex flex-row justify-content-between align-items-center">
                        <select class="form-control mr-1 designation"  name="designation"  required>
                           <?php foreach($opt_array as $options)
                           { 
                               ?>
                                    <option value="<?= $options ?>" <?php 
                                    if ($options == $row->designation) {
                                        echo 'selected'; 
                                       } ?>><?= $options?></option>
                           <?php 
                           }  ?>
                        </select>
                     </div>
                  </div>
               </div>
               <!-- End Designation -->

               <!-- Start Profile Image -->
               <div class="form-group mt-3">
                  <label for="formFile">Profile Image</label>
                  <input class="form-control" type="file" id="update_profile_img"  accept="images" name="profile_img">
                  <?php
                        if(file_exists('../public/assets/images/'.$row->profile_img) && $row->profile_img != null && $row->profile_img != ''){
                            $image = '../public/assets/images/'.$row->profile_img;
                        
                        }else{
                        
                            $image = '../public/assets/images/default.jpg';
                        }
                        ?>
                     Image: <img class="profile-img  mt-2" id="update_preview" src="<?= $image ?>" alt="<?= $image ?>"  height="40px" width="40px">
                  <input type="hidden" name="image_name" value="<?= $row->profile_img ?>" />
               </div>
               <!-- End Profile Image -->
               <!-- Start Submit Button -->
               <button class="btn btn-primary btn-block col-lg-2 mt-3" type="submit">Submit</button>
               <!-- End Submit Button -->
            </form>
            <!-- End Form -->
         </div>
         <!-- End Card Body -->
      </div>
      <!-- End Card -->
      <footer>
         <div class="my-4 text-muted text-center">
            <p>© Tridhya Tech Limited</p>
         </div>
      </footer>
      <!-- Optional JavaScript -->
      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
      <script src="../public/assets/js/vendors/popper.min.js"></script>
      <script src="../public/assets/js/vendors/bootstrap.min.js"></script>

   </body>
</html>