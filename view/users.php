<?php
include('../config/DbConfig.php');
   //Check if the user is logged in, if not then redirect him to login page
   if(!isset($_SESSION['login'])){
      header("Location: ../index.php");
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" href="../public/assets/css/style.css">
      <link href="../public/assets/css/vendors/bootstrap.min.css" rel="stylesheet">
      <script src="../public/assets/js/vendors/bootstrap.bundle.min.js" rel="text/javascript"></script>
      <link rel="stylesheet" href="../public/assets/fonts/all.min.css"/>
      <link rel="stylesheet" href="../public/assets/fonts/font-awesome.min.css"/>
      <script src="../public/assets/jquery/jquery-3.6.3.min.js"></script>
      <script src="../public/assets/js/user.js" rel="text/javascript"></script>
      <title>Users Data</title>
   </head>  
   <body>
      <header>
        <!-- navbar start -->
            <section class="nav-sec">
               <nav class="navbar navbar-expand-md navbar-dark bg-dark">
                  <div class="container-fluid">
                  <h3 class="h3 text-white text-center"><?php echo "Welcome ".$_SESSION['firstname']." ".$_SESSION['lastname']; ?></h3> 
                  <a  href="#"><img src="../public/assets/images/<?= $_SESSION['profile_img']?>" class="profile-img header_img float-end" height="40px" width="auto"/></a>
                  <a href="../view/log-out.php"><button class="btn btn-primary float-end">
                     &nbsp;LOG OUT</button></a> 
                  </div>
               </nav>
            </section>
        <!-- navbar end -->
      </header>
      <div class="container-fluid vw-100 vh-100">
        <div class="flash">
        </div>

         <div class="row">
            <h2 class="h2 text-center border text-success mt-5 bg-light">USER TABLE</h2>
            <a href="../view/add-user.php"><button class="btn btn-success mb-2 float-end">
           <i>&#43;</i> 
            &nbsp;ADD USER</button></a>
         </div>
      
         <div class="table-wrapper table-responsive px-2" tabindex="0">
            <table class="table table-bordered">
               <thead class="bg-light">
                  <tr>
                     <th>Sr.No</th>
                     <th>Name</th>
                     <th>Email</th>
                     <th>Mobile Number</th>
                     <th>Hobbies</th>
                     <th>Gender</th>
                     <th>Designation</th>
                     <th>Image</th>
                     <th>Action</th>
                  </tr>
               </thead>
               <tbody>
                  <?php
                     include "../controller/userController.php";
                     $list = new userController();
                     $result = $list->run('index');
                     $i = 1;

                  if ($result != 0) {
                     // output data of each row
                     foreach ($result as $row) { ?>
                     <tr> 
                        <td><?php echo $i; ?></td>
                        <td><?php echo $row["firstname"] . " " . $row["lastname"]; ?></td>
                        <td><?php echo $row["email"]; ?></td>
                        <td><?php echo $row["m_no"]; ?></td>
                        <td class="text-wrap"><?php echo implode(",", json_decode($row["hobbies"])); ?></td>
                        <td><?php echo $row["gender"]; ?></td>
                        <td><?php echo $row["designation"]; ?></td>
                        <?php
                        if (file_exists('../public/assets/images/' . $row['profile_img']) && $row['profile_img'] != null && $row['profile_img'] != '') {
                           $image = '../public/assets/images/' . $row['profile_img'];
                        } else {
                           $image = '../public/assets/images/default.jpg';
                        }
                        ?>
                        <td> <img class="profile-img" src="<?= $image ?>" alt="<?= $image ?>"  height="40px" width="40px"> </td>
                        <td>
                           <a href="../controller/userController.php?editid=<?php echo $row["id"] ?>"><button class="btn btn-warning">
                           &nbsp;Edit</button></a>
                           <a href="../controller/userController.php?uid=<?php echo $row["id"] ?>"><button class="btn btn-danger delete-action" data-id="<?= $row['id']; ?>">
                           &nbsp;Delete</button></a>
                        </td>
                     </tr><?php $i++;
                     }
                  }else { ?>
                     <tr>
                        <td colspan="9" class="text-center">No Data Found</td>
                     </tr>
                     <?php   }?>
               </tbody>
            </table>
         </div>
      </div>
   </body>
</html>