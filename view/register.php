<!DOCTYPE html>
<html>
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
      <link rel="stylesheet" href="../public/assets/css/style.css">
      <link href="../public/assets/css/vendors/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" href="../public/assets/fonts/all.min.css" />
      <script src="../public/assets/js/vendors/bootstrap.bundle.min.js" rel="text/javascript"></script>
      <script src="../public/assets/jquery/jquery-3.6.3.min.js" rel="text/javascript"></script>
      <script src="../public/assets/js/validation.js" rel="text/javascript"></script>
      <script src="../public/assets/js/common_function.js" rel="text/javascript"></script>
      <title>Register User</title>
   </head>
   <body class="container bg-light">
      <!-- Start Header form -->
      <div class="text-center pt-5 m-3">
         <h2>REGISTRATION FORM</h2>
      </div>
      <!-- End Header form -->
      <!-- Start Card -->
      <div class="card">
         <!-- Start Card Body -->
         <div class="card-body">
            <!-- Start Form -->
            <form id="register_form"  method="post" action="../controller/userController.php?register=1" enctype="multipart/form-data" class="needs-validation" novalidate autocomplete="on">
                  <div class="row">
                     <div class="col-6">
                        <div class="form-group mt-3">
                           <label class="h5 fw-normal">First Name</label>
                           <input type="text" class="form-control form-control-sm" name="firstname" id="firstname" placeholder="enter first name" required>
                           <div id="fname-error" class="form-group text-danger"></div>
                        </div>
                     </div>
                     <div class="col-6">
                        <div class="form-group mt-3">
                           <label class="h5 fw-normal">Last Name</label>
                           <input type="text" class="form-control form-control-sm" name="lastname" id="lastname" placeholder="enter last name" required>
                           <div id="lname-error" class="form-group text-danger"></div>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-6">
                        <div class="form-group mt-3">
                           <label class="h5 fw-normal" for="exampleInputEmail1">Email address</label>
                           <input type="email" name="email" class="form-control form-control-sm" id="email" aria-describedby="emailHelp" placeholder="enter email" required>
                           <div id="email-error" class="form-group"></div>
                        </div>
                     </div>
                     <div class="col-6">
                        <div class="form-group mt-3">
                           <label class="h5 fw-normal">Gender</label>
                           <br>
                           <input type="radio" class="gender" id="male" name="gender" value="male" checked>
                           <label class="form-check-label" for="male">Male</label>
                           <input type="radio" class="gender" id="female" name="gender" value="female">
                           <label class="form-check-label" for="female">Female</label>
                           <input type="radio" class="gender" id="other" name="gender" value="other">
                           <label class="form-check-label" for="other">Other</label>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-6">
                        <div class="form-group mt-3">
                           <label class="h5 fw-normal">Mobile Number</label>
                           <input type="text" class="form-control form-control-sm" name="m_no" id="m_no" placeholder="enter 10 digit only" pattern="\d{3}\d{3}\d{4}" required>
                           <div id="mno-error" class="form-group text-danger"></div>
                        </div>
                     </div>
                     <div class="col-6">
                        <div class="form-group mt-3">
                           <label class="h5 fw-normal">Hobbies</label><br>
                           <input class="hobbies" type="checkbox" name="hobbies[]" value="dancing" id="dancing">
                           <label class="form-check-label p-1" for="dancing">dancing</label>
                           <input class="hobbies" type="checkbox" name="hobbies[]" value="singing" id="singing">
                           <label class="form-check-label p-1" for="singing">singing</label>
                           <input class="hobbies" type="checkbox" name="hobbies[]" value="travelling" id="travelling">
                           <label class="form-check-label p-1" for="travelling">travelling</label>
                           <input class="hobbies" type="checkbox" name="hobbies[]" value="reading" id="reading">
                           <label class="form-check-label p-1" for="reading">reading</label>
                           <input class="hobbies" type="checkbox" name="hobbies[]" id="checkall">
                           <label class="form-check-label p-1" for="checkall">check all</label>
                           <div id="hobbies-error" class="form-group text-danger"></div>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-6">
                        <div class="form-group mt-3">
                           <label class="h5 fw-normal">Password</label>
                           <input type="password" class="form-control form-control-sm" name="password" id="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,16}" placeholder="enter password"  required> 
                           <div id="pw-error" class="form-group">
                           </div>
                        </div>
                     </div>
                     <div class="col-6">
                        <div class="form-group mt-3">
                           <label class="h5 fw-normal">Confirm Password</label>
                           <input type="password" class="form-control form-control-sm" name="cpassword" id="cpassword" placeholder="re-enter password" required>
                           <div id="pw-error" class="form-group text-danger">
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-6">
                        <div class="form-group mt-3">
                           <label class="h5 fw-normal">Designation</label>
                           <select class="form-select" name="designation" id="designation" required>
                              <option selected class="text-secondary" value="">please select designation</option>
                              <option value="employee">employee</option>
                              <option value="manager">manager</option>
                              <option value="assistant manager">assistant manager</option>
                              <option value="supervisor">supervisor</option>
                              <option value="hr">hr</option>
                              <option value="intern">intern</option>
                           </select>
                           <div id="designation-error" class="form-group text-danger"></div>
                        </div>
                     </div>
                     <div class="col-6">
                        <div class="form-group mt-3">
                           <label for="formFile" class="form-label h5 fw-normal">Profile Image</label>
                           <input class="form-control" type="file" id="profile_img" accept="images" name="profile_img">
                        </div>
                     </div>
                  </div>
                  <button type="submit" class="btn btn-primary btn-md mt-3" id="enter">Submit</button>
                  <p class="small fw-bold mt-2 pt-1 mb-0">Already have an account? <a href="../index.php"
                     class="link-danger">Login</a></p>
               </form>
            <!-- End Form -->
         </div>
         <!-- End Card Body -->
      </div>
      <!-- End Card -->
      <footer>
         <div class="my-4 text-muted text-center">
            <p>© Tridhya Tech Limited</p>
         </div>
      </footer>
      <!-- Optional JavaScript -->
      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
      <script src="../public/assets/js/vendors/bootstrap.bundle.min.js" rel="text/javascript"></script>
      <script src="../public/assets/js/vendors/popper.min.js"></script>
      <script src="../public/assets/js/vendors/bootstrap.min.js"></script>
   </body>
</html>