<?php

include('../model/User.php');

if(isset($_GET['add'])){
   $obj =  new userController;
    $obj->run('add');
}

if(isset($_GET['register'])){
    $obj =  new userController;
     $obj->run('register');
 }

if(isset($_GET['editid'])){
    $obj =  new userController;
     $obj->run('getUserDetail');
 }

if(isset($_GET['update'])){
    $obj =  new userController;
     $obj->run('update');
 }

 if(isset($_GET['uid'])){
    $obj =  new userController;
     $obj->run('delete');
 }

 if(isset($_GET['login'])){
    $obj =  new userController;
     $obj->run('login');
 }

class userController{

    public function run($action){
        switch($action)
        { 
            
            case "add" :
                $this->add();
                break;
            case "register" :
                $this->register();
                break;
            case "getUserDetail":
                $uid = $_GET['editid'];
                $this->getUserDetail($uid);
                break;
            case "update" :
                $this->update();
                break;
            case "delete" :
                $this->delete();
                break;
            case "login":
                $this->login();
                break;
            default:
                return $this->index();
                
        }
    }
    

    public function index(){
        //We create the user object
        $user = new User();
        $result = $user->list();
        return $result;
    }

    public function login(){
        $user = new User();
        $result = $user->login();
        if ($result!='ERROR') {
            echo 'User logged in succesfully.';
            header("Refresh: 1; URL = ../view/users.php");
        } else {
            echo 'Invalid Credentials!!!!';
            header("Refresh: 1; URL = ../index.php");
        }
    }

    public function add(){
        
        $user = new User();
        $result = $user->add();
        // ccd($result);
       
        if($result){
            echo 'User added succesfully.';
            header("Refresh: 1; URL = ../view/users.php");
        }else {
            echo "User not added";
        }
    }

    public function register(){
        
        $user = new User();
        $result = $user->add();
        // ccd($result);
        
        if($result){
            echo 'User registered succesfully.';
            header("Refresh: 1; URL = ../view/users.php");
            exit();
        }else {
            echo "User not added";
        }
    }
    public function getUserDetail($uid){
       
        $user = new User();
        $row = $user->getUserDetail($uid);
        $row = json_encode($row);

        require_once('../view/update-user.php');
        
    }
    public function update(){
        $user = new User();
        $result = $user->update();

        if ($result) {
            echo 'User updated succesfully.';
            header("Refresh: 1; URL = ../view/users.php");
        } else {
            echo "User not updated";
        }
    }
    
    public function delete(){
        $userId = $_GET['uid'];
        $user = new User();
        $result = $user->delete($userId);

        if ($result) {
            echo 'User deleted succesfully.';
            header("Refresh: 1; URL = ../view/users.php");
        } else {
            echo "User not deleted";
        }
    }
    
   
}



?>