-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 14, 2023 at 12:31 PM
-- Server version: 8.0.31
-- PHP Version: 8.1.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `php_assnt`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `firstname` varchar(20) NOT NULL,
  `lastname` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(20) NOT NULL,
  `m_no` bigint NOT NULL,
  `hobbies` varchar(60) NOT NULL,
  `gender` enum('male','female','other') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `designation` varchar(20) NOT NULL,
  `profile_img` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `is_deleted` enum('Y','N') NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=62 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `email`, `password`, `m_no`, `hobbies`, `gender`, `designation`, `profile_img`, `is_deleted`) VALUES
(1, 'Devanshi', 'Patel', 'test@example.com', 'sxcsdacsd', 8530303089, '[\"dancing\",\"singing\",\"travelling\"]', 'female', 'employee', 'user6.jpg', 'N'),
(27, 'Aemi', 'Shah', 'janvi.p@tridhyatech.com', 'janu', 8530303089, '[\"singing\",\"reading\"]', 'female', 'manager', 'user2.jpg', 'N'),
(3, 'ABC', 'Patel', 'joh@example.com', 'C SZC', 8530303089, '[\"dancing\",\"singing\"]', 'female', 'manager', 'user5.jpg', 'Y'),
(4, 'Divya', 'Patel', 'ddu000@gmail.com', 'aXXA', 8530303089, ' [\"singing\",\"travelling\",\"reading\"] ', 'female', 'manager', 'user4.jpg', 'Y'),
(31, 'Ami', 'Darji', 'tezast@example.com', 'ABC234@', 9875544443, '[\"dancing\",\"singing\",\"travelling\"]', 'female', 'assistant manager', 'default.png', 'N'),
(7, 'Aemi', 'Shah', 'john@example.com', 'scxasc', 8530303089, ' [\"dancing\",\"travelling\"] ', 'female', 'manager', 'user4.jpg', 'Y'),
(8, 'Aemi', 'Patel', 'atest@example.com', 'ZAza', 8530303083, '[\"singing\",\"travelling\"]', 'female', 'hr', 'default.jpg', 'Y'),
(9, 'Diya', 'Shah', 'aatest@example.com', 'ZaZxax', 8530303089, '[\"singing\",\"travelling\"]', 'female', 'supervisor', 'roses.jpg', 'N'),
(30, 'Aemi', 'Patel', 'john@example.com', 'jANVI45@', 8530303089, '[\"dancing\",\"singing\",\"travelling\",\"reading\"]', 'female', 'assistant manager', 'user4.jpg', 'N'),
(28, 'Diya', 'Darji', 'john@example.com', 'sxazaxax3$', 8530303089, '[\"dancing\",\"travelling\"]', 'female', 'manager', 'user3.jpg', 'Y'),
(32, 'Janvi', 'Patel', 'janu120@gmail.com', 'Janvi@123', 8530303089, '[\"dancing\",\"singing\",\"travelling\",\"reading\"]', 'female', 'supervisor', 'user1.jpg', 'N'),
(33, 'Sejal', 'Sanodiya', 'seju828@gmail.com', 'Sejal45@34', 9875544443, '[\"singing\",\"travelling\"]', 'female', 'hr', 'default.png', 'N'),
(57, 'Janvi', 'Darji', 'johncsx@example.com', 'Janvi@123', 8530303089, '[\"singing\",\"travelling\"]', 'female', 'assistant manager', 'user3.jpg', 'N'),
(35, 'Jaya', 'BJH', 'johsxan@example.com', 'Janvi@123', 8530303089, '[\"singing\"]', 'female', 'manager', 'flower1.jpg', 'N'),
(38, 'Diya', 'Patel', 'tegst@example.com', 'Janvi6767', 8766555532, '[\"singing\"]', 'female', 'assistant manager', 'user4.jpg', 'N'),
(37, 'Janvi', 'Patel', 'janvvi.p@tridhyatech.com', 'Janvi@123', 8530303089, '[\"travelling\"]', 'female', 'supervisor', 'user1.jpg', 'N'),
(60, 'Aemi', 'Patel', 'aemi88@gmail.com', 'Aemi@123', 9875544442, '[\"dancing\",\"singing\",\"travelling\"]', 'female', 'manager', 'user5.jpg', 'N'),
(61, 'Sejal', 'Sanodiya', 'sajel34@gmail.com', 'Sejal#123', 8530303089, '[\"dancing\",\"singing\",\"travelling\",\"reading\",\"on\"]', 'female', 'supervisor', 'femaleuser.jpg', 'N'),
(41, 'Aemi', 'Patel', 'seju88@gmail.com', 'Sejal23@', 987554444, ' [\"travelling\",\"reading\"] ', 'female', 'assistant manager', 'user5.jpg', 'N'),
(43, 'Ram', 'Sanodiya', 'test2@example.com', 'Ram123@', 9175755300, ' [\"dancing\",\"singing\",\"travelling\",\"reading\"] ', 'male', 'assistant manager', 'default.png', 'N'),
(50, 'Janvi', 'Patel', 'jjanvi.p@tridhyatech.com', 'Janvi$123', 8530303089, ' [\"travelling\",\"reading\"] ', 'female', 'supervisor', 'user1.jpg', 'N'),
(51, 'Ami', 'Shah', 'tesret@example.com', 'Janvi@234', 987554444, ' [\"travelling\"] ', 'female', 'manager', 'img2.png', 'N'),
(52, 'Jiya', 'Kothari', 'tesdst@example.com', 'Janvi@345', 87665555, ' [\"singing\"] ', 'female', 'hr', 'user2.jpg', 'N'),
(53, 'Janvi', 'Patel', 'janviiip@tridhyatech.com', 'Janvi@123', 8530303089, ' [\"reading\"] ', 'female', 'assistant manager', 'user4.jpg', 'N'),
(54, 'Janvi', 'Patel', 'janu123@example.com', 'Janvi@123', 8530303089, ' [\"singing\",\"travelling\"] ', 'female', 'hr', 'user4.jpg', 'N'),
(55, 'Aemi', 'Shah', 'janvipp@tridhyatech.com', 'Janvi@123', 9875544442, '[\"singing\",\"travelling\",\"reading\"]', 'female', 'assistant manager', 'default.jpg', 'N'),
(56, 'ABC', 'Shah', 'johcdn@example.com', 'Janvi@123', 8530303089, '[\"singing\",\"travelling\"]', 'female', 'assistant manager', 'roses.jpg', 'N'),
(58, 'Aemi', 'Sanodiya', 'jaanu000@gmail.com', 'Janvi@123', 8530303089, '[\"dancing\",\"singing\",\"travelling\"]', 'other', 'hr', 'user1.jpg', 'N');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
